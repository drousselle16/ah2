package exemplescours;

import formesgeo.Boite;
import formesgeo.Pave;
import formesgeo.Piece;

public class Exemple05 {
    
    public static void main(String[] args) {
           
        Boite uneBoite = new Boite();
        
        for (Piece p : uneBoite.getLesPieces()) {
        
          if (p instanceof Pave ){
          
              p.afficher();
          }      
        } 
    }
}


