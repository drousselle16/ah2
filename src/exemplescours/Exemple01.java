
package exemplescours;

import formesgeo.Cylindre;
import formesgeo.Pave;

public class Exemple01 {

    public static void main(String[] args) {
        
        Pave     pav1, pav2 ;
        
        pav1 = new Pave ("A", "vert", 7.87 , 1  , 3  , 4);
        pav2 = new Pave ("B","rouge", 4.43 , 1.5, 2.5, 2);
        
        Cylindre cyl1 = new Cylindre("C", "bleu", 2.70 , 2, 3);
        
        pav1.afficher();
        pav2.afficher();
        cyl1.afficher(); 
        
        System.out.println();
    }
}
