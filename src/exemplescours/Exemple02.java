package exemplescours;

import formesgeo.Boite;
import formesgeo.Piece;

public class Exemple02 {
  
    public static void main(String[] args) {

        Boite uneBoite= new Boite();
        
        for (Piece p : uneBoite.getLesPieces()){
        
             p.afficher();
        } 
        
        System.out.println();
    }
}

