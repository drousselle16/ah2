
package exercicestp;

import formesgeo.Cône;
import formesgeo.Sphère;

public class Exo01 {

    public static void main(String[] args) {
        Cône co1;
        co1 = new Cône("F" , "vert" , 4.43 , 2.50 , 3.50);
        co1.afficher();
        Sphère Sp1, Sp2;
        Sp1 = new Sphère("D","rouge",2.70,3.50);
        Sp2 = new Sphère("E","rouge",4.43,2.00);
        Sp1.afficher();
        Sp2.afficher();
        System.out.println();
        //Finis
    }
}
