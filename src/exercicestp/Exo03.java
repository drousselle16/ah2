
package exercicestp;
import formesgeo.Boite;
import formesgeo.Piece;

public class Exo03 {
    private static int poids;

    public static void main(String[] args) {
       Boite uneBoite= new Boite();
        
        for (Piece p : uneBoite.getLesPieces()){
            if(poids>100){
                p.afficher();
            }
        } 
       
        System.out.printf("\nPoids total des pièces: %5.2f g\n\n",uneBoite.poidsTotal());    
        System.out.println();
    }
}
