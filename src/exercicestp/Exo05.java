
package exercicestp;

import formesgeo.Boite;
import formesgeo.Piece;
import formesgeo.Sphère;

public class Exo05 {

    public static void main(String[] args) {
        Boite uneBoite= new Boite();
        
        for (Piece p : uneBoite.getLesPieces()){
            if(p instanceof Sphère){
                if(p.getCouleur().equals("rouge")){
                p.afficher();
            }
              
        }
        System.out.println();
    }
    }
}
