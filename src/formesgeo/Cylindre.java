 package formesgeo;
 import static java.lang.Math.*;
 
 public class Cylindre extends Piece {

   private double hauteur;
   private double rayon;
  
   public Cylindre( String repere, String couleur, double densite, double rayon, double hauteur ) {
  
       super(repere,couleur, densite);
       
       this.rayon=rayon;
       this.hauteur=hauteur;
  }

   @Override
   public double volume() { return PI * pow(rayon,2) * hauteur;}

   @Override
   public void afficher() {
       
   System.out.print("\nCYLINDRE ");
   
   super.afficher();
   
   System.out.printf("Rayon:  %6.2f cm  Hauteur:%6.2f cm\n", rayon , hauteur);
  
   System.out.printf("Volume: %6.2f cm3 Poids:  %6.2f g\n" , volume(), poids());
  }

   //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
   
   public double getHauteur() {
       return hauteur;
   }
   
   public void setHauteur(double hauteur) {
       this.hauteur = hauteur;
   }
   
   public double getRayon() {
       return rayon;
   }
   
   public void setRayon(double rayon) {
       this.rayon = rayon;
   }
   //</editor-fold>
 }




