package formesgeo;
import java.util.LinkedList;
import java.util.List;

public class Boite {

    private List<Piece> lesPieces= new LinkedList();

    public Boite(){

      Pave pav1,pav2;
      Cylindre cyl1;
      Cône co1;
      Sphère Sp1, Sp2;
      
      
      pav1  = new Pave ("A","vert", 7.87 , 1  , 3  , 4);
      pav2  = new Pave ("B","bleu", 4.43 , 1.5, 2.5, 2);
        
      cyl1  = new Cylindre("C", "rouge", 2.70 , 2, 3);  
      
      Sp1 = new Sphère("D","rouge",2.70,3.50);
      Sp2 = new Sphère("E","rouge",4.43,2.00);
            
      co1 = new Cône("G" , "vert" , 4.43 , 2.00 , 3.50);
      
      
        
      lesPieces.add(pav1);
      lesPieces.add(pav2);
      lesPieces.add(cyl1);
      lesPieces.add(Sp1);
      lesPieces.add(Sp2);
      lesPieces.add(co1);
      
    }

    public double poidsTotal(){
     
     double valRet=0.0;
     
     for(Piece p : this.lesPieces) {
     
         valRet += p.poids();
     }
     
     return valRet;
    }

    public List<Piece> getLesPieces() {
        
        return lesPieces;
    }
}



