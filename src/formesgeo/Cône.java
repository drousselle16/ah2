package formesgeo;
import static java.lang.Math.*;

public class Cône extends Piece {
    
    private double hauteur;
    private double rayon;
    
    public Cône( String repere, String couleur, double densite, double rayon, double hauteur ) {
  
       super(repere,couleur, densite);
       
       this.rayon=rayon;
       this.hauteur=hauteur;
       
  }
    @Override
    public double volume(){return PI * pow(rayon,2) * hauteur /3;}
    
    @Override
    public void afficher(){
        System.out.print("\nCÔNE ");
   
   super.afficher();
   
   System.out.printf("Rayon:  %6.2f cm  Hauteur:%6.2f cm\n", rayon , hauteur);
  
   System.out.printf("Volume: %6.2f cm3 Poids:  %6.2f g\n" , volume(), poids());
    }

    public double getHauteur() {
        return hauteur;
    }

    public void setHauteur(double hauteur) {
        this.hauteur = hauteur;
    }

    public double getRayon() {
        return rayon;
    }

    public void setRayon(double rayon) {
        this.rayon = rayon;
    }
    
}
